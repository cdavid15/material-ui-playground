import Apps from "../components/Apps";
import FullWidthGrid from "../components/Grid/FullWidthGrid";
import Cards from "../components/Card";
import SimplePaper from "../components/Paper/SimplePaper";
import CustomTimeline from "../components/Timeline/CustomTimeline";
import Dialog from "../components/Dialog";
import Device from "../components/Device";
// import BasicTimeline from "../components/Timeline/BasicTimeline";

import AppsIcon from "@material-ui/icons/Apps";
import ViewCompactIcon from "@material-ui/icons/ViewCompact";
import ViewCarouselIcon from "@material-ui/icons/ViewCarousel";
import NoteIcon from "@material-ui/icons/Note";
import TimelineIcon from "@material-ui/icons/Timeline";
import LaunchIcon from "@material-ui/icons/Launch";
import DevicesIcon from "@material-ui/icons/Devices";

const Routes = [
  {
    path: "/apps",
    sidebarName: "Apps",
    icon: <AppsIcon />,
    component: Apps,
  },
  {
    path: "/grid",
    sidebarName: "Grid",
    icon: <ViewCompactIcon />,
    component: FullWidthGrid,
  },
  {
    path: "/card",
    sidebarName: "Cards",
    icon: <ViewCarouselIcon />,
    component: Cards,
  },
  {
    path: "/paper",
    sidebarName: "Simple Paper",
    icon: <NoteIcon />,
    component: SimplePaper,
  },
  // {
  //   path: "/timeline-basic",
  //   sidebarName: "Timeline (Basic)",
  //   icon: <TimelineIcon />,
  //   component: BasicTimeline,
  // },
  {
    path: "/timeline",
    sidebarName: "Timeline",
    icon: <TimelineIcon />,
    component: CustomTimeline,
  },
  {
    path: "/dialog",
    sidebarName: "Dialog",
    icon: <LaunchIcon />,
    component: Dialog,
  },
  {
    path: "/device",
    sidebarName: "Device",
    icon: <DevicesIcon />,
    component: Device,
  },
];

export default Routes;
