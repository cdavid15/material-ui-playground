import React from "react";
import MediaCard from "./MediaCard";
import RecipeReviewCard from "./RecipeReviewCard";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "flex-start",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
}));

const Cards = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <RecipeReviewCard />
      <MediaCard />
    </div>
  );
};

export default Cards;
