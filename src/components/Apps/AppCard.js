import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Box,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Button,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  cardContent: {
    padding: theme.spacing(1),
  },

  media: {
    height: 140,
    padding: theme.spacing(2),
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    maxHeight: "100%",
    maxWidth: "100%",
    objectFit: "contain",
  },
  actions: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

export default function AppCard(props) {
  const classes = useStyles();

  return (
    <Card>
      <Box
        className={classes.media}
        style={{ backgroundColor: props.background }}
      >
        <CardMedia
          component="img"
          className={classes.image}
          image={props.image}
          title={props.name}
        />
      </Box>
      {props.name && (
        <CardContent className={classes.cardContent}>
          <Typography variant="h5" component="h2">
            {props.name}
          </Typography>
        </CardContent>
      )}
      <CardActions className={classes.actions}>
        <Button size="small" color="primary" variant="contained">
          Launch
        </Button>
      </CardActions>
    </Card>
  );
}
