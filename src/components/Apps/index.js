import React from "react";
import AppCard from "./AppCard";

import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "flex-start",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
}));

const Apps = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={6} sm={6} md={4} lg={2}>
          <AppCard
            // name="GitLab"
            image="https://about.gitlab.com/images/press/logo/png/gitlab-logo-white-rgb.png"
            background="#115293"
          />
        </Grid>
        <Grid item xs={6} sm={6} md={4} lg={2}>
          <AppCard
            // name="Mattermost"
            image="https://raw.githubusercontent.com/mattermost/mattermost-handbook/7150cebb78f87d7e1949e490de034d16b4e0069b/.gitbook/assets/branding/logoHorizontalWhite.png"
            background="#0058cc"
          />
        </Grid>
        <Grid item xs={6} sm={6} md={4} lg={2}>
          <AppCard
            // name="VS Code"
            image="https://blog.hpc.qmul.ac.uk/assets/images/visual_studio_code-card.png"
            background="#00000"
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default Apps;
