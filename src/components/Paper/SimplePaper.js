import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(1),
      width: theme.spacing(16),
      height: theme.spacing(16),
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
  },
}));

export default function SimplePaper() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Paper elevation={0}>0</Paper>
      <Paper>Default</Paper>
      <Paper elevation={3}>3</Paper>
      <Paper elevation={5}>5</Paper>
      <Paper elevation={7}>7</Paper>
      <Paper elevation={15}>15</Paper>
      <Paper elevation={24}>24</Paper>
    </div>
  );
}
