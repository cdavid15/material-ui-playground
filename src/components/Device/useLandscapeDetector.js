import { useState, useEffect } from "react";

import mobileDetector from "./mobileDetector";

const getOrientation = () => window.screen.orientation.type;

const useLandscapeDetector = () => {
  const [landscape, setLandscape] = useState(false);

  useEffect(() => {
    const userAgent =
      typeof navigator === "undefined" ? "SSR" : navigator.userAgent;

    const mobileDetect = mobileDetector(userAgent);

    const updateOrientation = (event) => {
      setLandscape(
        Boolean(
          mobileDetect.isMobile() &&
            Boolean(getOrientation().match(/landscape/i))
        )
      );
    };

    window.addEventListener("orientationchange", updateOrientation);
    return () => {
      window.removeEventListener("orientationchange", updateOrientation);
    };
  }, []);

  return landscape;
};

export default useLandscapeDetector;
