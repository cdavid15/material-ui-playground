import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import mobileDetector from "./mobileDetector";
import useScreenOrientation from "./useScreenOrientation";
import useLandscapeDetector from "./useLandscapeDetector";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "flex-start",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(2),
    },
  },
}));

const Device = () => {
  const classes = useStyles();
  const detectMobile = mobileDetector();
  const isAndroid = detectMobile.isAndroid();
  const isIOS = detectMobile.isIos();
  const isDesktop = detectMobile.isDesktop();
  const isMobile = detectMobile.isMobile();

  const screenOrientation = useScreenOrientation();

  const isLandscape = useLandscapeDetector();

  return (
    <div className={classes.root}>
      <ul>
        <li>
          isMobile() = <span>{isMobile.toString()}</span>
        </li>
        <li>
          isDesktop() = <span>{isDesktop.toString()}</span>
        </li>
        <li>
          isIOS() = <span>{isIOS.toString()}</span>
        </li>
        <li>
          isAndroid() = <span>{isAndroid.toString()}</span>
        </li>
        <li>
          Orientation = <span>{screenOrientation}</span>
        </li>
        <li>
          isLandscape = <span>{isLandscape.toString()}</span>
        </li>
      </ul>
    </div>
  );
};

export default Device;
